# syntax=docker/dockerfile:1.3
FROM docker.io/nixos/nix:2.11.0-amd64

ARG BUILD_DATE
ARG VCS_REF

# as per https://github.com/opencontainers/image-spec/blob/main/annotations.md,
# keep Label Schema labels for backward compatibility.
LABEL description="Container w/ nix + nixpkgs-fmt" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.vcs-url="https://git.dotya.ml/wanderer-containers/nix-nixpkgs-fmt.git" \
      org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.license=GPL-3.0 \
      org.opencontainers.image.title="nix-nixpkgs-fmt" \
      org.opencontainers.image.description="Container w/ nix + nixpkgs-fmt" \
      org.opencontainers.image.created=$BUILD_DATE \
      org.opencontainers.image.authors=wanderer \
      org.opencontainers.image.url="https://git.dotya.ml/wanderer-containers/nix-nixpkgs-fmt.git" \
      org.opencontainers.image.source="https://git.dotya.ml/wanderer-containers/nix-nixpkgs-fmt.git" \
      org.opencontainers.image.revision=$VCS_REF \
      org.opencontainers.image.licenses=GPL-3.0

RUN nix-env -iA nixpkgs.nixpkgs-fmt

# vim: ft=dockerfile
